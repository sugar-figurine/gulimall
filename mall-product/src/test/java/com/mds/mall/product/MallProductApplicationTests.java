package com.mds.mall.product;

import com.mds.mall.product.entity.BrandEntity;
import com.mds.mall.product.service.BrandService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class MallProductApplicationTests {

    @Autowired
    BrandService brandService;

    @Test
    void test() {
        BrandEntity brand = new BrandEntity();
        brand.setName("华为");
        brandService.save(brand);

    }

}
